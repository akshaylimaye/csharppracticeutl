﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kantisweets
{
    class SweetShop
    {
        //private bool shopOpenOrClose;
        private int noOfCustomers;
        private int revenue = 0, cost;
        private int sweets, count;

        public SweetShop(int sweets, int cost)
        {
            this.sweets = sweets;
            count = 0;
            this.cost = cost;
        }

        public static void getCustomer()
        {
            SweetShop pro = new SweetShop(50, 10);
            pro.display("Mention number of customers- ");
            pro.noOfCustomers = pro.getInput();
            pro.display("Number of customers- " + pro.noOfCustomers);
            pro.getSweets();
        }

        public void getSweets()
        {
            int cust_sweets;
            for (int i = 0; i < noOfCustomers; i++)
            {
                display("Customer No:" + (i + 1));
                display("Enter Number Of Sweets: ");
                cust_sweets = getInput();
                sweetCounter(cust_sweets);
                display("Number Of Sweets: " + cust_sweets);
                display("Cost = " + (cost * cust_sweets));
                display("Remaining Sweets: " + sweets);
            }
            display("\n\n\n");
            display("Final Revenue Generated: " + revenue);
            display("Final Sweets Remaining: " + sweets);
            display("Total Sweets Sold: " + count);
            Console.ReadLine();
        }


        public void sweetCounter(int cust_sweets)
        {
            int sweets1;
            if (sweets < cust_sweets)
            {
                sweets1 = cust_sweets - sweets;
                sweets += sweets1;
            }
            sweets = sweets - cust_sweets;
            revenue = revenue + (cust_sweets * cost);
            count = count + cust_sweets;
        }

        public void summary()
        {
            display("\n\n\n");
            display("Final Revenue Generated: " + revenue);
            display("Final Sweets Remaining: " + sweets);
            display("Total Sweets Sold: " + count);
        }
        public void display(String anything)
        {
            Console.WriteLine(anything);
        }

        public int getInput()
        {
            int anyNumber = Convert.ToInt32(Console.ReadLine());
            return anyNumber;
        }
    }
}
