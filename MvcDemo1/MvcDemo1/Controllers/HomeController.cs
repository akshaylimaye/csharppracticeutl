﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcDemo1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message1 = "wHATEVER YOU wANT";
            ViewBag.Message2 = "ASUDGHO";
            ViewBag.Message3 = "ASFOUHA ASH";

            return View();
        }

        public ActionResult Superhero()
        {
            var list = GetFiveSuperHeroes();
            return View(list.ToList());
        }

        public static List<Superhero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<Superhero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new Superhero();

            name = "Deadpool";
            power = "Immoratal";
            brand = "Marvel";
            age = 99;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);
            tempHero = new Superhero();


            name = "Saktiman";
            power = "desi power";
            brand = "DD";
            age = 8;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);
            tempHero = new Superhero();

            name = "krish";
            power = "son of rohit";
            brand = "koi mil gaya";
            age = 31;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);
            tempHero = new Superhero();

            name = "Spiderman";
            power = "web";
            brand = "Marvel";
            age = 25;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);
            tempHero = new Superhero();

            name = "Thor";
            power = "Hammer";
            brand = "Marvel";
            age = 89;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);
            tempHero = new Superhero();

            name = "Superman";
            power = "Immortal";
            brand = "DC";
            age = 66;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);
            tempHero = new Superhero();


            return tempList;
        }
    }
}